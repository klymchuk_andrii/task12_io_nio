package com.klymchuk.model;

import java.io.IOException;

public interface Model {
    String showShip() throws IOException, ClassNotFoundException;
    String readBuffer() throws IOException;
}
