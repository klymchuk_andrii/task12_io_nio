package com.klymchuk.model;

import java.io.Serializable;

public class Droids implements Serializable {
    private String name;
    private int power;
    transient private int age;

    public Droids(String name, int power, int age) {
        this.name = name;
        this.power = power;
        this.age = age;
    }

    @Override
    public String toString() {
        return " droid: " + name + " power: " + power + " age: " + age;
    }
}
