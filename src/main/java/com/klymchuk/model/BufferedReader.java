package com.klymchuk.model;

import java.io.*;

public class BufferedReader {
    public String readBuffer() throws IOException {
        String answer;
        answer="Read 1 byte: ";
        DataInputStream in = new DataInputStream(
                new BufferedInputStream(
                        new FileInputStream(
                                "D:\\універ\\3.1\\тзі\\ПетровАА_КомпБезопасность.pdf")));
        byte b;
        int count=0;
        try {
            while (true) {
                b = in.readByte();
                count++;
            }
        }catch (EOFException e){
        };
        in.close();
        answer+=count;
        return answer;
    }
    public String readBufferOneMb() throws IOException {
        String answer;
        int bufferSize=1024*1024;
        answer="\nRead 1 mb: ";
        DataInputStream in = new DataInputStream(
                new BufferedInputStream(
                        new FileInputStream(
                                "D:\\універ\\3.1\\тзі\\ПетровАА_КомпБезопасность.pdf"),bufferSize));
        byte b;
        int count =0;
        try {
            while (true) {
                b = in.readByte();
                count++;
            }
        }catch (EOFException e){
        };
        in.close();
        answer+=count;
        return answer;
    }

}
