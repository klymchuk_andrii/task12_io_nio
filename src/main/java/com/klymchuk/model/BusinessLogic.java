package com.klymchuk.model;

import java.io.*;

public class BusinessLogic implements Model{
    private Ship ship;
    private BufferedReader bufferedReader;

    @Override
    public String showShip() throws IOException, ClassNotFoundException {
        StringBuilder sb = new StringBuilder();

        sb.append("write to file");

        ship = new Ship("sun",
                new Droids("r2",23,10));
        FileOutputStream fileOutputStream =
                new FileOutputStream("D:\\epam\\task12_io_nio\\ship.ser");
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(ship);
        objectOutputStream.close();

        sb.append("\nwrote to file");
        sb.append("\nread from file");

        FileInputStream fileInputStream =
                new FileInputStream("D:\\epam\\task12_io_nio\\ship.ser");
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        Ship newShip = (Ship)objectInputStream.readObject();

        sb.append("\nread from file\n").append(newShip);

        return sb.toString();
    }

    @Override
    public String readBuffer() throws IOException{
        bufferedReader = new BufferedReader();
        return bufferedReader.readBuffer()+bufferedReader.readBufferOneMb();
    }
}
