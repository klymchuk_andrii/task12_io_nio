package com.klymchuk.model;

import java.io.Serializable;

public class Ship implements Serializable {
    private String name;
    private Droids droid;

    public Ship(String name, Droids droid) {
        this.name = name;
        this.droid = droid;
    }

    @Override
    public String toString() {
        return "Ship: " + name + droid;
    }
}
