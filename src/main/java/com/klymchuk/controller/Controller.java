package com.klymchuk.controller;

import java.io.IOException;

public interface Controller {
    String showShip()throws IOException,ClassNotFoundException;
    String readBuffer() throws IOException;
}
