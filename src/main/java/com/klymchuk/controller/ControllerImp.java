package com.klymchuk.controller;

import com.klymchuk.model.BusinessLogic;
import com.klymchuk.model.Model;

import java.io.IOException;

public class ControllerImp implements Controller {
    Model model;

    public ControllerImp(){
        model = new BusinessLogic();
    }

    @Override
    public String showShip() throws IOException, ClassNotFoundException {
        return model.showShip();
    }

    @Override
    public String readBuffer() throws IOException {
        return model.readBuffer();
    }
}
