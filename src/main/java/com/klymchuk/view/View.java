package com.klymchuk.view;

import com.klymchuk.controller.Controller;
import com.klymchuk.controller.ControllerImp;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.*;

public class View {
    private Controller controller;

    private Logger logger;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input;

    public View() {
        controller = new ControllerImp();
        logger = LogManager.getLogger(View.class);

        input = new Scanner(System.in);

        setMenu();

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1",this::showShip);
        methodsMenu.put("2",this::readBuffer);
    }

    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - show ship");
        menu.put("2", "2 - read from buffer");
        menu.put("3", "3 - set value to unknowing field");
        menu.put("4", "4 - get information");
    }

    private void Menu() {
        logger.info("\nMENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            Menu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                logger.info(e);
            }
        } while (!keyMenu.equals("Q"));
    }

    private void showShip(){
        try {
            logger.info(controller.showShip());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void readBuffer(){
        try{
            logger.info(controller.readBuffer());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
